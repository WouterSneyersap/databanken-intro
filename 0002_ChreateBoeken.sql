USE modernways;
CREATE TABLE boeken(
	Voornaam VARCHAR(50) CHAR SET UTF8MB4,
	Familienaam VARCHAR(80) CHAR SET UTF8MB4,
	Titel VARCHAR(255) CHAR SET UTF8MB4,
	Stad VARCHAR(50) CHAR SET UTF8MB4,
	Verschijningsjaar VARCHAR(4),
	Uitgeverij VARCHAR(80) CHAR SET UTF8MB4,
	Herdruk VARCHAR(4),
	Commentaar VARCHAR(1000)
);